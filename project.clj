(defproject weberia "1.0.0"
  :description "Weberia - A framework for pragmatic web systems"
  :url "http://github.com/weberia"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repositories [["bigdata" "http://www.systap.com/maven/releases"]
                 ["nxparser" "http://nxparser.googlecode.com/svn/repository"]
                 ["sonatype" "https://oss.sonatype.org/content/repositories/releases"]
                 ["apache" "http://repository.apache.org/content/repositories/releases"]]
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [ring/ring-core "1.4.0"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [ns-tracker "0.3.0"]
                 [org.clojure/clojurescript "1.7.122"]
                 [enfocus "2.1.1"]
                 [domina "1.0.3"]
                 [compojure "1.4.0"]
                 [lein-marginalia "0.8.0"]
                 [com.bigdata/bigdata "1.5.2"]]
  :plugins [[lein-ring "0.9.6"]
            [lein-cljsbuild "1.0.6"]]
  :jvm-opts ["-Djsse.enableSNIExtension=false"]
  :cljsbuild {
              :builds [{
                        ; The path to the top-level ClojureScript source directory:
                        :source-paths ["src-cljs"]
                        ; The standard ClojureScript compiler options:
                        ; (See the ClojureScript compiler documentation for details.)
                        :compiler {
                                   :output-to "resources/public/js/main.js"
                                   :optimizations
                                   :whitespace
                                   :pretty-print true}}]}
  :ring {:handler weberia.core/app}
  :profiles {:dev {:dependencies [[ring/ring-mock "0.2.0"]]}})
