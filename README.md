# Project Weberia - Weberia Framework


## Usage

This project consists of client side (ClojureScript) and server side (Clojure). Clojurescript has to be compiled first:

```bash
$ lein cljsbuild once
Successfully compiled "resources/public/js/main.js" in 8.841 seconds.
$
```
You'll see the result in "resources/public/js/main.js"

To serve as a web application:

```bash
$ lein ring server-headless
```
If you don't want ``lein`` to automatically open the web browser. If you want to run the web browser:

```bash
$ lein ring server
```
REPL available using this command:

```bash
$ lein trampoline cljsbuild repl-rhino
Running Rhino-based ClojureScript REPL.
To quit, type: :cljs/quit
cljs.user=>
```

## Libaries


## License

Copyright © 2015 [http://bpdp.name](Bambang Purnomosidi D. P.)

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
