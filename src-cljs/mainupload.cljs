(ns mainupload
  (:require [enfocus.core :as ef]
            [enfocus.events :as events])
  (:require-macros [enfocus.macros :as em]))

(em/defaction change [msg]
  ["#button1"] (ef/content msg))

(em/defaction setup []
  ["#button1"]  (events/listen :click #(change "I have been clicked")))

(set! (.-onload js/window) setup)
